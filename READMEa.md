# go-ops


## Purpose

The goal of this repository is to deploy the go-epfl service with Ansible, it is made to deploy docker and the services as well as some repositories on 3 remote servers.

## Usage

```
#./gosible
```

### Examples

### Only on one host

```
./gosible  --prod
./gosible  --qual
./gosible  --test
#./gosible  --dev
```

### Specifying a tag

```
./gosible --test -t "test"
```

### Specifying multiple tags

```
./gosible --test -t "tag1,tag2,tag3"
```

### Skip specifying a tag

```
./gosible --test --skip-tags "github_key_import"
```

### Skip specifying multiple tags

```
./gosible --test -skip-tags "tag1,tag2,tag3"
```


## Notes

