- name: Prometheus container
  community.docker.docker_container:
    name: go_prometheus
    image: prom/prometheus
    state: started
    recreate: >-
      {{ "go.prometheus.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.external-url=/prometheus'
      - '--web.route-prefix=/'
    volumes:
      - '{{ APP_DIRECTORY_ROOT }}/docker/monitoring/prometheus:/etc/prometheus'
      - '{{ DATA_DIRECTORY_HOST }}/prometheus:/prometheus'
    # This has to be removed as soon as c2c e2em is authenticated
    exposed_ports:
      - "9090"
    # ports:
    #   - "9090:9090"
    user: '1000:33'
    networks:
      - name: "{{ external_docker_network_name }}"
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '50m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.prometheus
    - go.prometheus.force-reload

- name: Create a node-exporter container
  community.docker.docker_container:
    name: go_node-exporter
    image: prom/node-exporter
    state: started
    recreate: >-
      {{ "go.node-exporter.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.ignored-mount-points=^/(sys|proc|dev|host|etc)($$|/)'
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    # exposed_ports:
    #   - '9100'
    # ports:
    #   - '9100:9100'
    networks:
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '10m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.node-exporter
    - go.node-exporter.force-reload

- name: Create cadvisor container
  community.docker.docker_container:
    name: go_cadvisor
    image: gcr.io/cadvisor/cadvisor:v0.46.0
    state: started
    recreate: >-
      {{ "go.cadvisor.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:ro
      - /sys:/sys:ro
      - /var/lib/docker:/var/lib/docker:ro
      - /cgroup:/cgroup:ro
      - /dev/disk/:/dev/disk:ro
    privileged: true
    exposed_ports:
      - '8080'
    # ports:
    #   - '8888:8080'
    networks:
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '10m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.cadvisor
    - go.cadvisor.force-reload

- name: Create PostgreSQL exporter
  community.docker.docker_container:
    name: go_postgres-exporter
    image: quay.io/prometheuscommunity/postgres-exporter
    state: started
    recreate: >-
      {{ "go.postgres-exporter.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    env:
      DATA_SOURCE_NAME: 'postgresql://{{ POSTGRES_USER }}:{{ POSTGRES_PASSWORD }}@go_postgres:5432/postgres?sslmode=disable'
    networks:
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '50m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.postgres-exporter
    - go.postgres-exporter.force-reload

- name: Create nginx exporter container
  community.docker.docker_container:
    name: go_nginx-exporter
    image: nginx/nginx-prometheus-exporter:0.9.0
    state: started
    recreate: >-
      {{ "go.nginx-exporter.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    entrypoint:
      - "/usr/bin/nginx-prometheus-exporter"
      - "-nginx.scrape-uri"
      - "http://go_nginx:8002/nginx_status"
    networks:
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '10m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.nginx-exporter
    - go.nginx-exporter.force-reload

- name: Create php fpm exporter container
  community.docker.docker_container:
    name: go_php-fpm-exporter
    image: bakins/php-fpm-exporter:v0.6.1
    state: started
    recreate: >-
      {{ "go.php-fpm-exporter.force-reload" in ansible_run_tags }}
    restart_policy: 'unless-stopped'
    command:
      #- '--help-long'
      - '--addr=0.0.0.0:9253'
      - '--endpoint=http://go_nginx:8002/php-fpm-status'
      - '--web.telemetry-path=/metrics'
    networks:
      - name: "{{ internal_docker_network_name }}"
    log_driver: 'json-file'
    log_options:
      max-size: '10m'
      max-file: '5'
  tags:
    - go.monitoring.force-reload
    - go.php-fpm-exporter
    - go.php-fpm-exporter.force-reload
