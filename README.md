# Go Ops

This repository is the [Ops] part of go.epfl.ch. Using [Ansible] and the
[Ansible suitcase], it aims to deploy, within Docker, go.epfl.ch in **every
environments**, including the operator's laptop.

Please have a look to the [main repository], especially [README.md] and
[ARCHITECTURE.md] for a better understanding.

Please note that the Ansible code in this repository only targets Ubuntu
operating system and might not work on over flavors.

## Usage

**TL;DR**
```
./gosible
```

Use the `--dev` command line argument to deploy the environment locally (i.e. on
the operator's laptop).
Use the `--test` command line argument to deploy the environment on the test
(EPFL's XaaS virtual machine).
Use the `--qual` command line argument to deploy the environment on the quality
(switch engine virtual machine, external to EPFL).
Use the `--prod` command line argument to deploy the environment on the
production (EPFL's XaaS virtual machine).

### Maintenance mode

The Go application includes a minimal mode (a kv file containing redirects is 
dumped local and a basic web server serve the redirects). The maintenance mode
can be activated the following way:
1. Be sure to update go-minimal with `gosible -t go_minimal`
2. Run gosible with the extra var `maintenance_mode=true`. This command can be
   limited to traefik (`gosible -e maintenance_mode -t traefik`)




[Ops]: https://en.wikipedia.org/wiki/Data_center_management#Operations
[Ansible]: https://www.ansible.com/
[Ansible suitcase]: https://github.com/epfl-si/ansible.suitcase
[README.md]: https://gitlab.com/epfl-isasfsd/go-epfl/-/blob/master/README.md
[ARCHITECTURE.md]: https://gitlab.com/epfl-isasfsd/go-epfl/-/blob/master/ARCHITECTURE.md
[main repository]: https://gitlab.com/epfl-isasfsd/go-epfl
